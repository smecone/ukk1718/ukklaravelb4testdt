<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookLending extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'books_lendings';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * This Lend BelongsTo Book
     */
    public function book()
    {
        return $this->belongsTo('App\Book', 'book_id');
    }

    /**
     * This BookLend hasOne BookReturn
     */
    public function return()
    {
        return $this->hasOne('App\BookReturn', 'loan_id', 'id');
    }
}
