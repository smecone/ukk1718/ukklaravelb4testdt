<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {        
        switch($guard) {
            case 'web-officer':
            case 'api-officer':                
                if (Auth::guard($guard)->check()) {
                    return redirect(route('officer.home'));
                }
                break;    
            case 'web-member':
            case 'api-member':
                if (Auth::guard($guard)->check()) {
                    return redirect(route('member.home'));
                }
                break;
            default:
                return $next($request);
                break;
        }

        return $next($request);
    }
}
