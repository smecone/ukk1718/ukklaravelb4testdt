<?php

namespace App\Http\Controllers\Traits\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait AuthController {
    /**
     * Reset api_token when user Logout
     */
    protected function resetToken() {        
        $current = $this->guard()->user();
        $current->api_token = str_random(80);
        $current->save();
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {        
        $this->resetToken();

        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }
}