<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class PivotBookCategory extends Pivot
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'books_categories';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
}
