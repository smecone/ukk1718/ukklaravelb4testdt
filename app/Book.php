<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'books';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * BelongsToMany Categories
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category', 'categories_books', 'book_id', 'category_id')->using('App\PivotBookCategory')->withTimestamps();
    }

    /**
     * Has Many Book Lendings
     */
    public function lendings()
    {
        return $this->hasMany('App\BookLending', 'book_id', 'id');
    }    
}
