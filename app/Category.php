<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * BelongsToMany Books
     */
    public function books()
    {
        return $this->belongsToMany('App\Book', 'categories_books', 'category_id', 'book_id')->using('App\PivotBookCategory')->withTimestamps();
    }
}
