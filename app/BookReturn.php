<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookReturn extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'books_returns';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    
    /**
     * This BookReturn BelongsTo BookLending
     */
    public function lending()
    {
        return $this->belongsTo('App\BookLenging', 'loan_id');        
    }
}
