<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::prefix('/officer')->group(function () {
    Route::middleware(['auth:api-officer'])->group(function () {  
        Route::namespace('Officer')->group(function() {
            Route::get('/book/list', 'BookController@list')->name('api.officer.book.list');
        });        
    });
});
