<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('index');

Route::prefix('officer')->group(function () {
    Route::namespace('Auth\Officer')->group(function () {
        Route::get('/login', 'LoginController@showLoginForm')->name('officer.login');
        Route::post('/login', 'LoginController@login');
        Route::middleware(['auth:web-officer'])->group(function () {
            Route::get('/logout', 'LoginController@logout')->name('officer.logout');
        });
    });

    Route::middleware(['auth:web-officer'])->group(function () {
        Route::get('/', function () {            
            return view('officer.home');
        })->name('officer.home');
        
        Route::namespace('Officer')->group(function () {
            Route::get('/book', 'BookController@index')->name('officer.book');
        });
    });  
});

Route::prefix('member')->group(function () {
    Route::namespace('Auth\Member')->group(function () {
        Route::get('/login', 'LoginController@showLoginForm')->name('member.login');
        Route::post('/login', 'LoginController@login');
        Route::middleware(['auth:web-member'])->group(function () {
            Route::get('/logout', 'LoginController@logout')->name('member.logout');
        });
    });

    Route::middleware(['auth:web-member'])->group(function () {        
        Route::get('/', function () {
            return view('member.home');
        })->name('member.home');
        Route::get('/profile', function () {

        });
    });
});