@extends('main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="card-panel grey lighten-5">
                <h3>Officer Login</h3>
                <form class="" id="form" action="{{ route('officer.login') }}" method="POST">
                    <div class="row">                        
                        {{method_field('post')}}
                        {{csrf_field()}}
                        
                        <div class="input-field col s6">
                            <input name="username" id="username" type="text" class="validate">
                            <label for="username">Username</label>
                        </div>
                        <div class="input-field col s6">
                            <input name="password" id="password" type="password" class="validate">
                            <label for="password">Password</label>
                        </div>
                    </div>
                    <hr/>
                    <button class="waves-effect waves-light btn" type="submit" name="action">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $('#form').submit(function (event) {
            $.ajax({
                type: "POST",
                url: $(this).action,
                data: $(this).serialize(),
            }).done(function(response) {
                console.log(response)
                M.toast({
                    html: response.message, 
                    outDuration: 2000, 
                    classes: 'rounded',
                    completeCallback: function () {
                        location.assign(response.redirectTo)
                    }
                })
            }).fail(function(error) {
                console.log(error)                
                M.toast({
                    html: error.responseJSON.message, 
                    outDuration: 3000,
                    classes: 'rounded'
                })
            })
            event.preventDefault();
        })
    </script>
@endsection