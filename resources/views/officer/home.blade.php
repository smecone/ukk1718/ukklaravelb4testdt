@extends('main')

@section('title', 'Officer Home')
@section('sidenav', 'true')
@section('content')
    <div class="">
        <div class="card-panel teal white-text">
            <table id="table-book" class="highlight responsive-table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>ISBN</th>
                        <th>Title</th>                        
                        <th>Stock</th>
                        <th>Categories</th>
                        <th colspan="4">Actions</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <ul id="table-book-paginate" class="pagination">
                <li class="disabled"><a href="#!"><</a></li>
                <li class="active"><a href="#!">1</a></li>
                <li class="active"><a href="#!">1</a></li>
                <li class="waves-effect"><a href="#!">></li>
            </ul>                
        </div>
    </div>
@endsection

@section('js')
    <script>
        function borrow(id) {
            alert(id)
        }
        function edit(id) {

        }
        function destroy(id) {

        }
        $(document).ready(function () {            
            $.get({
                url: "{{ route('api.officer.book.list') }}",
                headers: {
                    'Authorization': window.Laravel.api_token
                }
            }).done(function(response) {
                console.log(response)
                responseX.forEach(function (item) {
                    var row = 
                    "<tr>"
                        +"<td>"+item.id+"</td>"
                        +"<td>"+item.isbn+"</td>"
                        +"<td>"+item.title+"</td>"
                        +"<td>"+item.stock+"</td>"
                        +"<td>"+item.categories+"</td>"
                        +"<td>"+"<a onClick='borrow("+item.id+")' class='btn-small waves-effect waves-light green'>Borrow</a>"+"</td>"
                        +"<td>"+"<a onClick='edit("+item.id+")' class='btn-small waves-effect waves-light orange'>Edit</a>"+"</td>"
                        +"<td>"+"<a onClick='destroy("+item.id+")' class='btn-small waves-effect waves-light red'>Delete</a>"+"</td>"                        
                    +"</tr>"                    
                    $("#table-book").find("tbody").append(row)
                })

                var paginate = $("#table-book-paginate")
                paginate.append()
            })
        })
    </script>
@endsection