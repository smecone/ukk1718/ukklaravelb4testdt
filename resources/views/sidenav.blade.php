<ul class="collection with-header">
    @auth('web-officer')
        <li class="collection-header light-green white-text">
            <h4>Hello, {{ Auth::guard('web-officer')->user()->name }}</h4>
            <span class="badge red white-text">Officer</span>        
        </li>
    @endauth
    @auth('web-member')
        <li class="collection-header light-green white-text">        
                <h4>Hello, {{ Auth::guard('web-member')->user()->nis }}</h4>            
                <span class="badge green white-text">Member</span>        
        </li>
    @endauth
    @auth('web-officer')
        <div>
            <a class="collection-item" href="{{ route('officer.book') }}">Books</a>
            <a class="collection-item" href="{{ route('index') }}">Books Lending</a>
            <a class="collection-item" href="{{ route('index') }}">Books Return</a>            
            <a class="collection-item" href="{{ route('index') }}">Categories</a>
            <a class="collection-item" href="{{ route('index') }}">Members</a>
            <a class="collection-item" href="{{ route('index') }}">Officers</a>
        </div>
    @endauth             
</ul>