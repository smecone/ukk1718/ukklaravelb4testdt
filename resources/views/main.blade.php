<!DOCTYPE html>
<html lang="en">
<head>
    <!--Stylesheet-->
    <link rel="stylesheet" href="{{ url('/') }}/css/materialize.css" media="screen,projection">
    <link rel="stylesheet" href="{{ url('/') }}/css/main.css">

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }} - @yield('title')</title>    
</head>
<body>    
    <!-- Navbar goes here -->    
    <nav>        
        <div class="nav-wrapper light-green">
            <div class="container">
                <a href="#" id="brand" class="brand-logo">Logo</a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li>
                        <a href="{{route('index')}}">Home</a>
                    </li>
                    <li>
                        <a href="{{route('officer.home')}}">Officer</a>
                    </li>
                    <li>
                        <a href="{{route('member.home')}}">Member</a>
                    </li>                
                    @auth('web-officer')
                        <li>
                            <a href="{{route('officer.logout')}}">Logout</a>
                        </li>
                    @endauth
                    @auth('web-member')
                        <li>
                            <a href="{{route('member.logout')}}">Logout</a>
                        </li>
                    @endauth                
                </ul>
            </div>
        </div>
    </nav>        

    <!-- Page Layout here -->
    @hasSection('sidenav')
        <div class="row">
            <div class="col s3">
                <!-- Grey navigation panel -->
                @include('sidenav')
            </div>

            <div class="col s9">
                <!-- Teal page content  -->            
                <!-- Content -->
                @yield('content')
            </div>
        </div>
    @else
        @yield('content')
    @endif

    

<!-- Javascript -->
<script type="text/javascript">
    window.Laravel = {
        csrf_token: '{{ csrf_token() }}',
        baseUrl: '{{ url('/') }}',
        @auth
        api_token: 'Bearer {{ Auth::user()->api_token }}'
        @endauth
    }
</script>
<script src="{{ url('/') }}/js/lib/jquery-3.3.1.js"></script>
<script src="{{ url('/') }}/js/lib/materialize.js"></script>
{{--  <script src="{{ url('/') }}/js/config.js"></script>
<script src="{{ url('/') }}/js/require.js"></script>
<script data-main="{{ url('/') }}/js/config.js" src="{{ url('/') }}/js/require.js"></script>
<script src="{{ url('/') }}/js/main.js"></script>  --}}
<script>
    $(document).ready(function (){        
    })    
</script>
@yield('js')
</body>
</html>