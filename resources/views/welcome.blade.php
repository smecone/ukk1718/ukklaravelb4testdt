@extends('main')

@section('title', 'Welcome')

@section('content')
    <div class="container">
        <div class="card-panel teal darken-3 white-text">
            <p>
                Lorem Ipsum Lorem IpsumLorem IpsumLorem IpsumLorem IpsumLorem IpsumLorem Ipsum
                Lorem IpsumLorem IpsumLorem IpsumLorem IpsumLorem IpsumLorem IpsumLorem Ipsum
                Lorem IpsumLorem IpsumLorem IpsumLorem IpsumLorem IpsumLorem IpsumLorem Ipsum
                Lorem IpsumLorem IpsumLorem IpsumLorem IpsumLorem IpsumLorem IpsumLorem Ipsum
            </p>
        </div>
    </div>
@endsection

@section('js')
<script>
    require(['config'], function() {
        require(['jquery', 'materialize'], function($, materialize) {
            console.log($)
            console.log(materialize)
        })
    })
</script>
@endsection