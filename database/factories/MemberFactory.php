<?php

use Faker\Factory as Faker;

$factory->define(App\Member::class, function () {
    $faker = Faker::create(config('app.locale'));

    return [
        'nis' => $faker->randomNumber(4),
        'name' => $faker->name,
        'password' => $faker->password,
        'class' => $faker->word,
        'api_token' => str_random(80),
    ];
});
