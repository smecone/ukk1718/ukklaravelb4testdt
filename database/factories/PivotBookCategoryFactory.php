<?php

use Faker\Generator as Faker;

$factory->define(App\PivotBookCategory::class, function (Faker $faker) {
    return [
        'book_id' => function() use ($faker) {
            return App\Book::find(
                $faker->biasedNumberBetween(1, App\Book::count())
            )->id;
        },
        'category_id' => function() use ($faker) {
            return App\Category::find(
                $faker->biasedNumberBetween(1, App\Category::count())
            )->id;
        },
    ];
});
