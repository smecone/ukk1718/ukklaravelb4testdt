<?php

use Faker\Generator as Faker;
use Carbon\Carbon as Carbon;

$factory->define(App\BookReturn::class, function (Faker $faker) {
    $book_lending_returned = App\BookLending::find(
        $faker->unique()->biasedNumberBetween(1, App\BookLending::count())
    );
    $book_lending_returned->status = "returned";
    $book_lending_returned->save();

    $loan_id = $book_lending_returned->id;
    $date_loan = Carbon::parse($book_lending_returned->date_loan);
    $date_return = $book_lending_returned->date_period;
    $fine = 0;
    if (($out_period=$date_loan->diffInDays(Carbon::parse($date_return))) > 3)
    {
        $fine = $out_period * 500;
    }

    return [
        'date_return' => $date_return,
        'fine' => $fine,
        'loan_id' => $loan_id,
        'member_id' => function () use ($faker) {
            return App\Member::find(
                $faker->biasedNumberBetween(1, App\Member::count())
            )->id;
        },
        'officer_id' => function () use ($faker) {
            return App\Officer::find(
                $faker->biasedNumberBetween(1, App\Officer::count())
            )->id;
        },
    ];
});
