<?php

use Faker\Factory as Faker;

$factory->define(App\Book::class, function () {
    $faker = Faker::create(config('app.locale'));

    return [
        'isbn' => $faker->isbn13,
        'title' => $faker->sentence,
        'stock' => $faker->randomNumber(3)        
    ];
});
