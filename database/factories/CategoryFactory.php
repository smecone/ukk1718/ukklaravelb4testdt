<?php

use Faker\Factory as Faker;

$factory->define(App\Category::class, function () {
    $faker = Faker::create(config('app.locale'));

    return [
        'name' => $faker->word,
        'description' => $faker->text,
    ];
});
