<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        // $this->call(UsersTableSeeder::class);

        $this->call([
            OfficerTableSeeder::class,
            MemberTableSeeder::class,
            CategoryTableSeeder::class,
            BookTableSeeder::class,
            BookLendingTableSeeder::class,
            BookReturnTableSeeder::class,
        ]);
    }
}
