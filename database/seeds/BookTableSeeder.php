<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        $faker = Faker::create();
        factory(App\Book::class, 30)->create()->each(function ($book) use ($faker) {            
            factory(App\PivotBookCategory::class, $faker->randomDigit)->create();
        });
    }
}
