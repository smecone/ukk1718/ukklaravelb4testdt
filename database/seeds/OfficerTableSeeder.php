<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class OfficerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Create First Officer
         */
        $fo = new App\Officer;
        $fo->name = "Officer";
        $fo->username = "firstofficer";
        $fo->password = Hash::make("free4you");
        $fo->api_token = str_random(80);
        $fo->save();

        factory(App\Officer::class, 50)->create();            
    }
}
