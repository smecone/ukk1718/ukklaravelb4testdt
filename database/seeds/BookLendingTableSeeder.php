<?php

use Illuminate\Database\Seeder;

class BookLendingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\BookLending::class, 50)->create();
    }
}
