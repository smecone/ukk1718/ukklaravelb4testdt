<?php

use Illuminate\Database\Seeder;

class MemberTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Create First Member
         */
        $fm = new App\Member;
        $fm->name = "Member";
        $fm->nis = "8426";        
        $fm->password = Hash::make("free4you");
        $fm->class = "RPL";
        $fm->api_token = str_random(80);
        $fm->save();

        factory(App\Member::class, 50)->create();
    }
}
