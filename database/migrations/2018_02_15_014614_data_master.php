<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DataMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('officers', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('username');
            $table->string('password');
            $table->string('api_token', 80)->unique();
            $table->rememberToken();

            $table->timestamps();
        });

        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nis');
            $table->string('name');
            $table->string('password');
            $table->string('class');
            $table->string('api_token', 80)->unique();
            $table->rememberToken();
            
            $table->timestamps();
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->text('description');
            
            $table->timestamps();
        });        

        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');

            $table->string('isbn');
            $table->string('title');
            $table->integer('stock');
            
            $table->timestamps();
        });

        Schema::create('books_categories', function (Blueprint $table) {
            // Many to Many Relations . Categories with Books
            $table->integer('book_id')->unsigned()->nullable();
            $table->foreign('book_id')->references('id')->on('books')->onDelete('cascade');
            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            
            $table->timestamps();
        });

        Schema::create('books_lendings', function (Blueprint $table) {
            $table->increments('id');

            $table->dateTime('date_loan');
            $table->dateTime('date_period');
            $table->enum('status', ['borrowed', 'returned']);
            $table->integer('book_id')->nullable()->unsigned();
            $table->integer('member_id')->nullable()->unsigned();
            $table->integer('officer_id')->nullable()->unsigned();

            // Relations            
            $table->foreign('book_id')->references('id')->on('books');
            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('officer_id')->references('id')->on('officers');
            
            $table->timestamps();
        });

        Schema::create('books_returns', function (Blueprint $table) {
            $table->increments('id');

            $table->dateTime('date_return');
            $table->integer('fine');
            $table->integer('loan_id')->nullable()->unsigned()->unique();
            $table->integer('member_id')->nullable()->unsigned();
            $table->integer('officer_id')->nullable()->unsigned();

            // Relations
            $table->foreign('loan_id')->references('id')->on('books_lendings');
            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('officer_id')->references('id')->on('officers');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books_returns');
        Schema::dropIfExists('books_lendings');
        Schema::dropIfExists('books');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('members');
        Schema::dropIfExists('officers');
    }
}
