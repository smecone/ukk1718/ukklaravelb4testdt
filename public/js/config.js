var config = {
    baseUrl: window.Laravel.baseUrl+'/js/lib',
    paths: {        
        jquery: 'jquery-3.3.1',
        materialize: 'materialize'
    }
}

if (typeof requirejs === "undefined" || typeof require === "undefined") {
    var require = config
} else {    
    requirejs.config(config)
    require.config(config)
}
